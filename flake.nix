{
  description = "A flake for wget2";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.flake-compat = { url = "github:edolstra/flake-compat"; flake = false; };
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-compat, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      my_gnulib = pkgs.gnulib.overrideAttrs (oldAttrs: rec {
        version = "20210208";
        src = pkgs.fetchgit {
          url = "https://git.savannah.gnu.org/r/gnulib.git";
          rev = "0b38e1d69f03d3977d7ae7926c1efeb461a8a971";
          sha256 = "06bj9y8wcfh35h653yk8j044k7h5g82d2j3z3ib69rg0gy1xagzp";
        };
      });
    in
      rec {
        packages = {
          wget2 = pkgs.stdenv.mkDerivation
            rec {
              src = ./.;

              _ac_init_regex = " *AC_INIT *\\( *\\[ *([^] ]+) *] *, *\\[ *([^] ]+) *].*";
              _configure = pkgs.lib.splitString "\n" (builtins.readFile ./configure.ac);
              _ac_init = builtins.filter (x: x != null) (builtins.map (builtins.match _ac_init_regex) _configure);
              pname = builtins.elemAt (builtins.head _ac_init) 0;
              version = builtins.elemAt (builtins.head _ac_init) 1;

              buildInputs =
                with pkgs; [
                  brotli bzip2 gpgme libidn2 libpsl xz nghttp2 openssl pcre2 zlib zstd
                ];
              nativeBuildInputs = with pkgs; [ autoreconfHook flex lzip pkg-config python3 texinfo ];

              autoreconfPhase = ''
                # copy gnulib into build dir and make writable.
                # Otherwise ./bootstrap copies the non-writable files from nix store and fails to modify them
                rm -rf gnulib
                cp -r ${my_gnulib} gnulib
                chmod -R u+w gnulib/{build-aux,lib}

                ./bootstrap --no-git --gnulib-srcdir=gnulib --skip-po
              '';
              configureFlags = [
                "--with-ssl=openssl"
              ];
              # wget2_noinstall contains forbidden reference to /build/
              postPatch = ''
                substituteInPlace src/Makefile.am \
                  --replace 'bin_PROGRAMS = wget2 wget2_noinstall' 'bin_PROGRAMS = wget2'
              '';
            };
        };
        defaultPackage = packages.wget2;
        devShell = packages.wget2;
      });
}
